const ADD_USER = 'ADD_USER'
const UPDATE_SEARCH = 'UPDATE_SEARCH';
const FETCH_USER_BEGIN = 'FETCH_USER_BEGIN'
const FETCH_USER_ERROR = 'FETCH_USER_ERROR'
const FETCH_USER_SUCESS = 'FETCH_USER_SUCESS'
// const FILTER_TODO='FILTER_TODO'


const intialUser = {
    user: [],
    loading: false,
    error: '',
    searchText: ''
}

export const addUser = (taskName) => {
    return {
        type: ADD_USER,
        payLoad: taskName
    }
}

export const fetchUser = () => {
    return dispatch => {
        dispatch(fetchUserBegin())
        return fetch('https://jsonplaceholder.typicode.com/users')
            .then(res => res.json())
            .then(data => {
                console.log(data)
                dispatch(fetchUserSucess(data))

            })
            .catch(error => dispatch(fetchUserError(error)))
    }
}

export const fetchUserBegin = () => {
    return {
        type: FETCH_USER_BEGIN
    }
}
export const fetchUserSucess = user => {
    return {
        type: FETCH_USER_SUCESS,
        payLoad: user
    }
}
export const fetchUserError = error => {
    return {
        type: FETCH_USER_ERROR,
        payLoad: error
    }
}
export const updateSearch = (data) => {
    return {
        type: UPDATE_SEARCH,
        payLoad: data
    }
}

export const userReducer = (state = intialUser, action) => {
    switch (action.type) {
        case ADD_USER:
            const newTodos = action.payLoad
            return [newTodos, ...state]
        case FETCH_USER_BEGIN:
            return { ...state, loading: true }
        case FETCH_USER_SUCESS:
            return { ...state, user: action.payLoad, loading: false, error: '' }
        case FETCH_USER_ERROR:
            return { ...state, loading: false, error: action.payLoad }
        case UPDATE_SEARCH:
            // console.log(action.payLoad)
            return { ...state, searchText: action.payLoad }
        default:
            return state
    }
}
