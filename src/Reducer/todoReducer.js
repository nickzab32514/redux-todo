const ADD_TODO = 'ADD_TODO';
const FETCH_TODO_BEGIN = 'FETCH_TODO_BEGIN';
const FETCH_TODO_ERROR = 'FETCH_TODO_ERROR';
const FETCH_TODO_SUCESS = 'FETCH_TODO_SUCESS';
const UPDATE_SEARCH_TODO = 'UPDATE_SEARCH_TODO';
const FILTER_TODO = 'FILTER_TODO';
const CHANGE_DONE = 'CHANGE_DONE'



const intialtodos = {
    todos: [],
    loading: false,
    error: '',
    searchText: '',
    filter: -1


}

export const addTodo = (taskName) => {
    return {
        type: ADD_TODO,
        payLoad: taskName
    }
}
export const filterTodo = (data) => {
    return {
        type: FILTER_TODO,
        payLoad: data
    }
}

export const updateSearchTodo = (data) => {
    return {
        type: UPDATE_SEARCH_TODO,
        payLoad: data
    }
}
export const changeDone = (valueClick) => {
    return {
        type: CHANGE_DONE,
        payLoad: valueClick
    }
}

export const fetchTodo = (userId) => {
    return dispatch => {
        dispatch(fetchTodoBegin())
        return fetch('https://jsonplaceholder.typicode.com/todos?userId=' + userId)
            .then(res => res.json())
            .then(data => {
                dispatch(fetchTodoSucess(data))
            })
            .catch(error => dispatch(fetchTodoError(error)))
    }
}

export const fetchTodoBegin = () => {
    return {
        type: FETCH_TODO_BEGIN
    }
}
export const fetchTodoSucess = todos => {
    return {
        type: FETCH_TODO_SUCESS,
        payLoad: todos
    }
}
export const fetchTodoError = error => {
    return {
        type: FETCH_TODO_ERROR,
        payLoad: error
    }
}

export const todoReducer = (state = intialtodos, action) => {
    switch (action.type) {
        case ADD_TODO:
            const newTodos = action.payLoad
            // const newTodos={taskName:newTodoName}
            return [newTodos, ...state]
        case FETCH_TODO_BEGIN:
            return { ...state, loading: true }
        case FETCH_TODO_SUCESS:
            return { ...state, todos: action.payLoad, loading: false, error: '' }
        case FETCH_TODO_ERROR:
            return { ...state, loading: false, error: action.payLoad }
        case FILTER_TODO:
            return { ...state, filter: action.payLoad }
        case UPDATE_SEARCH_TODO:
            return { ...state, searchText: action.payLoad }
        case CHANGE_DONE:
            const temp = [...state.todos];
            for (let i = 0; i < temp.length; i++) {
                if (temp[i].id == action.payLoad) {
                    temp[i].completed = true;
                }
            }
            state.todos = temp;
            return { ...state }
        default:
            return state
    }
}