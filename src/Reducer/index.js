import { combineReducers } from 'redux'
import { counterReducer } from './counterReducer'
import { todoReducer } from './todoReducer'
import { userReducer } from './userReducer'
import { todoUserReducer } from './todoUserReducer'
import { loginReducer } from './loginReducer'



// export const rootReducer =combineReducers({
//     counter:counterReducer
// })

// export const rootReducer =comcineReducers({
//     counterReducer:counterReducer
// })
export const rootReducer = combineReducers({
    counter: counterReducer,
    todosCompState: todoReducer,
    userToTodosCompState: todoUserReducer,
    userCompState: userReducer,
    storeLogin:loginReducer

})