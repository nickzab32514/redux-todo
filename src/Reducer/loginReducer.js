export const LOGIN = 'LOGIN'
export const LOGOUT = 'LOGOUT'

const initialLoginState = {
    isLogin: false
}

export const loginProcess = () => {
    return { type: LOGIN }
}
export const logoutProcess = () => {
    return { type: LOGOUT }
}

export const loginReducer = (state = initialLoginState, action) => {
    switch (action.type) {
        case LOGIN:
            return { ...state, isLogin: true }
        case LOGOUT:
            return { ...state, isLogin: false }
        default:
            return state
    }
}