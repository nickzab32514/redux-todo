const intialCount = 0

export const counterReducer = (state = intialCount, action) => {
    switch (action.type) {
        case 'INCREMENT':
            return state + action.payLoad
        case 'DECREMENT':
            return state - action.payLoad
        default:
            return state
    }
}
