const FETCH_USER_TODO_BEGIN = 'FETCH_USER_TODO_BEGIN';
const FETCH_USER_TODO_ERROR = 'FETCH_USER_TODO_ERROR';
const FETCH_USER_TODO_SUCESS = 'FETCH_USER_TODO_SUCESS';

const intialtodoUser = {
    users: {},
    loading: false,
}


export const fetchUserTodo = (userId) => {
    return dispatch => {
        dispatch(fetchUserBegin())
        return fetch('https://jsonplaceholder.typicode.com/users/' + userId)
            .then(res => res.json())
            .then(data => {

                dispatch(fetchUserSucess(data))
            })
            .catch(error => dispatch(fetchUserError(error)))
    }
}

export const fetchUserBegin = () => {
    return {
        type: FETCH_USER_TODO_BEGIN
    }
}
export const fetchUserSucess = users => {
    return {
        type: FETCH_USER_TODO_SUCESS,
        payLoad: users
    }
}
export const fetchUserError = error => {
    return {
        type: FETCH_USER_TODO_ERROR,
        payLoad: error
    }
}

export const todoUserReducer = (state = intialtodoUser, action) => {
    switch (action.type) {
        case FETCH_USER_TODO_BEGIN:
            return { ...state, loading: true }
        case FETCH_USER_TODO_SUCESS:
            return { ...state, users: action.payLoad, loading: false }
        case FETCH_USER_TODO_ERROR:
            return { ...state, loading: false }

        default:
            return state
    }
}