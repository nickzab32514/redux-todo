import React from 'react';
import './App.css';
import TodoRedux from './components/TodoRedux';
import UserRedux from './components/UserRedux';
import { logoutProcess, loginProcess } from './Reducer/loginReducer';
import { BrowserRouter, Route, Redirect } from 'react-router-dom';
import PrivateRoute from './PrivateRoute';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
// import HeadersTopic from './components/HeadersTopic';
import LoginPage from './components/LoginPage';


const App = (props) => {

  const { loginProcess, logoutProcess, isLogin } = props
  console.log(isLogin)
  return (
    <BrowserRouter>
      <Route path="/processLogin"
        render={() => {
          loginProcess()
          return <Redirect to="/users"></Redirect>
        }} />
      <Route path="/processLogout"
        render={() => {
          logoutProcess()
          return <Redirect to="/"></Redirect>
        }} />
      <Route path="/" component={LoginPage} exact={true} />
      {/* <Route path="/loginPage" component={LoginPage} exact={true}  /> */}
      <PrivateRoute path="/users" component={UserRedux} exact={true} />
      <PrivateRoute path="/users/:userid/todo" component={TodoRedux} />
    </BrowserRouter>
  );
}


const mapStateToProps = state => {
  return {
    isLogin: state.storeLogin.isLogin

  }
}
const mapDispatchToProps = dispatch => {
  return bindActionCreators({ loginProcess, logoutProcess }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(App);

