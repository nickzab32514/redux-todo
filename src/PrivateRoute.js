import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

const PrivateRoute=({path,component:Component,exact,isLogin})=>(
    <Route path={path} exact={exact} render={(props) =>{
        // const isLogin=localStorage.getItem('isLogin')
        console.log(isLogin)
        if(isLogin==true)
        return <Component {...props}/>
        else
        return <Redirect to="/"></Redirect>

    }} /> 

)

const mapStateToProps = state => {
    return {
      isLogin:state.storeLogin.isLogin

    }
  }
  
  export default connect(mapStateToProps, null)(PrivateRoute);
