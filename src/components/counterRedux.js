import React from 'react';
import { connect } from 'react-redux';
import { decrement, increment } from '../counterAction/counterAction';
import { bindActionCreators } from 'redux';

// const CounterRedux = () => {

//     const counter = useSelector(state => state.counter)
//     const dispatch = useDispatch();

//     return (
//         <div>
//             <div>Counts={counter}</div>
//             <div>
//                 {/* <button onClick={() => dispatch(decrement())}>-</button>
//                 <button onClick={() => dispatch(increment())}>+</button> */}
//                 <button onClick={() => dispatch(decrement(2))}>-</button>
//                 <button onClick={() => dispatch(increment(2))}>+</button>
//             </div>
//         </div>
//     )
// }

const CounterRedux = (props) => {

    const { counter, increment, decrement } = props
    return (
        <div>
            <div>Counts={counter}</div>
            <div>
                <button onClick={() => decrement(2)}>-</button>
                <button onClick={() => increment(2)}>+</button>
            </div>
        </div>
    )
}

const mapStateToProps = state => {
    return {
        counter: state.counter
    }
}

const mapDispatchToProps = dispatch => {
    return bindActionCreators({ increment, decrement }, dispatch)

}

export default connect(mapStateToProps,mapDispatchToProps )(CounterRedux);
