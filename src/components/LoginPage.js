import React from 'react';
import {useHistory}from 'react-router';
import { connect } from 'react-redux';
import { logoutProcess, loginProcess } from '../Reducer/loginReducer';
import { bindActionCreators } from 'redux';

const LoginPage = () => {
    // const history=useHistory()
    return (
        <div class="contanier">
            <h1>login page</h1>
            <form>
                <div class="row">
                    <label class="col-sm-1" for="exampleInputEmail1">Email address</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" />
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-1" for="exampleInputPassword1">Password</label>
                    <div class="col-sm-8">
                        <input type="password" class="form-control" id="exampleInputPassword1" />
                    </div>
                </div>
                <input type="button" value="submit" onClick={
                    () => {
                        window.location=( "/processLogin")
                    }
                }
                />
            </form>
        </div>
    )
}

const mapStateToProps = state => {
    return {
        isLogin: state.storeLogin.isLogin

    }
}
const mapDispatchToProps = dispatch => {
    return bindActionCreators({ loginProcess, logoutProcess }, dispatch)
}


export default connect(mapStateToProps, mapDispatchToProps)(LoginPage);
