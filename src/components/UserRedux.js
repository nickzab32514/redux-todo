import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { fetchUser } from '../Reducer/userReducer';
import UserList from './UserList'

const UserRedux = () => {
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(fetchUser())
    }, [])
    
    return(
        <div>
            <div>
                <UserList/>
            </div>
        </div>
    )
}
export default UserRedux