import React, { useEffect } from 'react';

import TodoList from './TodoList';
import TodoForm from './TodoForm';

const TodoRedux = (props) => {

    const userId=props.match.params.userid

    return(
        <div>
            <div>
                <TodoForm userId={userId}/>
                <TodoList  userId={userId}/>
            </div>
        </div>
    )
}
export default TodoRedux