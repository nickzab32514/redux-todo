import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { List, Typography, Button, Input, Select, PageHeader } from 'antd';
import { updateSearchTodo, fetchTodo, filterTodo, changeDone } from '../Reducer/todoReducer'
import { bindActionCreators } from 'redux';

const { Search } = Input;
const { Option } = Select;


const TodoList = (props) => {
    // const todos = useSelector(state => state.todosCompState.todos)
    // const todos = useSelector(state => state.todosCompState.data)
    const { todos, searchText, fetchTodo, updateSearchTodo, changeDone, filterTodo, filterSelected, userId } = props
    // console.log("TCL: TodoList -> filterSelected", filterSelected)

    // const userId=props.match.params.user_id
    useEffect(() => {
        fetchTodo(userId)
        console.log(fetchTodo(userId))
    }, [])

    const setSearch = (event) => {
        updateSearchTodo(event.target.value)
    }
    const setTypeTodos = (value) => {
        filterTodo(value)
    }
    return (
        <div>
            <PageHeader style={{ backgroundColor: '#F7F5C2', margin: "10px" }}>
                <Search
                    placeholder="input search user"
                    // onSearch={value => console.log(value)}
                    style={{ width: 200 }}
                    onChange={setSearch}
                />
                <Select defaultValue={-1} style={{ width: 120 }} onChange={setTypeTodos}>
                    <Option value={-1}>ALL</Option>
                    <Option value={true}>DONE</Option>
                    <Option value={false}>DOING</Option>
                </Select>
                <PageHeader style={{ backgroundColor: 'white', margin: "10px" }}>
                    <List
                        header={<div class="row">
                            <th>To DoList</th>
                        </div>}
                        bordered
                        dataSource=
                        {

                            filterSelected == -1 && searchText == '' ?
                                todos
                                :
                                filterSelected == -1 ?
                                    todos.filter(e => e.title.match(searchText))
                                    :
                                    todos.filter(e => e.completed == filterSelected && e.title.match(searchText))

                        }
                        renderItem={(item, index) => (
                            <List.Item>
                                <div class="col-1">
                                    {item.completed ?
                                        <Typography.Text delete>Done</Typography.Text>
                                        :
                                        <Typography.Text mark >Doing</Typography.Text>
                                    }
                                </div>
                                <div class="col-5">
                                    {item.title}
                                </div>
                                {item.completed ?
                                    <div></div>

                                    :
                                    <div class="col-5">
                                        <Button onClick={() => changeDone(item.id)} type="basic">Done</Button>
                                    </div>
                                }
                            </List.Item>
                        )}
                    />
                </PageHeader>
            </PageHeader>
        </div>

    )
}
const mapStateToProps = state => {
    return {
        todos: state.todosCompState.todos,
        searchText: state.todosCompState.searchText,
        filterSelected: state.todosCompState.filter

    }
}
const mapDispatchToProps = dispatch => {
    return bindActionCreators({ fetchTodo, updateSearchTodo, filterTodo, changeDone }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(TodoList);
