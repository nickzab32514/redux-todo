import React from 'react';
import { connect, useDispatch } from 'react-redux';
import { List, Skeleton, Avatar, Input, Button, Icon, PageHeader } from 'antd';
import { updateSearch } from '../Reducer/userReducer'
import { useHistory } from 'react-router';
const { Search } = Input;


const UserList = (props) => {
    const history = useHistory();
    const dispatch = useDispatch();
    const { user, searchText } = props

    const setSearch = (event) => {
        dispatch(updateSearch(event.target.value))
    }
    console.log(searchText)

    return (

        <div style={{ backgroundColor: '#C9F3AA', padding: 24, }}>
            <PageHeader

                title="UserPage"
                extra={[
                    <Button type="primary"
                        onClick={() => history.push('/')}>
                        <Icon type="left" />
                        LogOut
                </Button>,
                ]}
            >
            </PageHeader>
            <PageHeader style={{ backgroundColor: '#F9F9C2' }}>
                <Search
                    placeholder="input search user"
                    style={{ width: 200 }}
                    onChange={setSearch}
                />
                <List
                    className="demo-loadmore-list"
                    itemLayout="horizontal"
                    dataSource={
                        searchText == "" ?
                            user
                            :
                            user.filter(e => e.name.match(searchText))
                    }
                    renderItem={item => (
                        <List.Item
                            actions={[
                                <a onClick={() => history.push('/users/' + item.id + '/todo')}>toDo</a>
                            ]}

                        >
                            <Skeleton avatar title={false} loading={item.loading} active>
                                <List.Item.Meta
                                    avatar={
                                        <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
                                    }
                                    title={<a href="https://ant.design">{item.name}</a>}
                                    description={<div class="row">
                                        <div>Email:{item.email}</div>
                                        <div>Phone{item.phone}</div>
                                    </div>}

                                />
                            </Skeleton>

                        </List.Item>
                    )}
                />
            </PageHeader>
        </div>
    )
}
const mapStateToProps = state => {
    return {
        user: state.userCompState.user,
        searchText: state.userCompState.searchText
    }
}

export default connect(mapStateToProps)(UserList);
