import React, { useEffect } from 'react';
import { Descriptions, PageHeader, Button, Icon } from 'antd';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { useHistory } from 'react-router';
import { fetchUserTodo } from '../Reducer/todoUserReducer'

const TodoForm = (props) => {
    const { usersList, fetchUserTodo, userId } = props
    const history = useHistory();

    useEffect(() => {
        fetchUserTodo(userId)
    }, [])

    return (
        <div>
            <PageHeader title={<h1>Todo Redux</h1>}
                style={{ backgroundColor: '#C9F3AA' }}
                extra={[
                    <Button type="primary"
                        onClick={() => history.push('/users')}>
                        <Icon type="left" />
                        Back
                </Button>
                ]}>
                <Descriptions title={"Infomation Of Todo" + usersList.name}>
                    <Descriptions.Item label="UserName">{usersList.name}</Descriptions.Item>
                    <Descriptions.Item label="Telephone">{usersList.phone}</Descriptions.Item>
                    <Descriptions.Item label="Address">{usersList.email}</Descriptions.Item>
                </Descriptions>
            </PageHeader>
        </div>
    )
}

const mapStateToProps = state => {
    console.log(state.userToTodosCompState.users)
    return {
        usersList: state.userToTodosCompState.users
    }
}
const mapDispatchToProps = dispatch => {
    return bindActionCreators({ fetchUserTodo }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(TodoForm);